/**
 * Created by tcd on 25.10.15.
 */
angular.module('app')
    .controller('MainController', ['$scope', 'User', 'MockUserApi', function ($scope, User, MockUserApi) {
        'use strict';
        $scope.usersSelected = [];
        $scope.editDialogShown = false;
        $scope.deleteDialogShown = false;
        $scope.init = function () {
            //mocking server data, due to real API just not exist
            $scope.getAllUsers();
        };

        /**
         * edit dialog
         * @param Array users
         */
        $scope.editDialog = function (users) {
            $scope.editDialogShown = true;
        };

        $scope.toggleSelection = function (userIndex) {
            var user = $scope.users[userIndex];
            if ($scope.findInSelected(user.Id) !== null) {
                $scope.usersSelected.splice(userIndex,1);
            } else {
                $scope.usersSelected.push(user);
            }

        };

        $scope.findInSelected = function(id) {
            if ($scope.usersSelected.length === 0) return null;
            $scope.usersSelected.forEach(function(selected, index) {
                if (selected.Id === id) {
                    return selected;
                } else {
                    return null;
                }
            });
        };

        $scope.dismissEditDialog = function () {
            $scope.editDialogShown = false;
        };

        $scope.deleteDialog = function () {
            $scope.deleteDialogShown = true;
        };

        $scope.dismissDeleteDialog = function () {
            $scope.deleteDialogShown = false;
        };

        $scope.getAllUsers = function () {
            User.findAll().$promise.then(function (users) {
                $scope.users = users;
            });
        };
        $scope.saveChanges = function () {
            //I hate making AJAX requests in loops, but there is only way due to API specification
            $scope.usersSelected.forEach(function (user) {
                User.save(user);
            });
            $scope.editDialogShown = false;
            $scope.getAllUsers();
        };

        $scope.deleteSelectedUsers = function () {
            $scope.usersSelected.forEach(function (user) {
                User.remove(user);
            });
            $scope.deleteDialogShown = false;
            $scope.getAllUsers();
        };
        $scope.init();
}]);