/**
 * Created by tcd on 25.10.15.
 */
angular.module('app', ['ui.router', 'ngResource', 'ngMockE2E']);

angular.module('app')
    .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
        'use strict';
        $urlRouterProvider.otherwise('users');

        $stateProvider.state('users', {
            url: '/users',
            templateUrl: 'src/templates/main.html',
            controller: 'MainController'
        });
    }])
    .run(['$httpBackend', function ($httpBackend) {
        $httpBackend.whenGET('src/templates/main.html').passThrough();
    }]);
