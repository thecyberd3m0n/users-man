angular.module("app")
    .factory('LocalFileUser', ['$resource', function ($resource) {
        return $resource('users.json/:id', {id: '@id'}, {
            getData: {
                method: 'GET',
                isArray: true
            }
        });
    }]);