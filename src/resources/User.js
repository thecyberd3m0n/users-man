/**
 * Created by tcd-linux on 26.10.15.
 */
angular.module("app")
    .factory('User', ['$resource', function ($resource) {
        var host = 'http://users.impaqgroup.com/';
        return $resource(host, {id: '@Id'}, {
            findAll: {
                url: host + 'findall',
                method: 'GET',
                isArray: true
            },
            find: {
                url: host + 'find/:id',
                method: 'GET',
                isArray: true
            },
            save: {
                url: host + 'edit/:id',
                method: 'POST'
            },
            remove: {
                url: host + 'remove/:id',
                method: 'POST'
            }
        });
    }]);