/**
 * Created by tcd-linux on 28.10.15.
 */
angular.module('app')
    .service('MockUserApi', ['$httpBackend', function ($httpBackend) {
        'use strict';
        var Dima = {
            "Id": "12",
            "Name": "Dima",
            "Surname": "Rysanow",
            "DateOfBirth": "25-04-1991",
            "MobileNumber": "502329777",
            "Address": "Wroclaw, Hiszpanska 21"
        };
        $httpBackend.whenGET('http://users.impaqgroup.com/findall')
            .respond([Dima]);
        $httpBackend.whenGET('http://users.impaqgroup.com/find/12')
            .respond(Dima);
        $httpBackend.whenPOST('http://users.impaqgroup.com/remove/12').respond(200);
        $httpBackend.whenPOST('http://users.impaqgroup.com/edit/12',
            Dima)
            .respond(Dima);
        this.getDima = function () {
            return Dima;
        };

        this.flush = function () {
            $httpBackend.flush();
        };
    }]);