/**
 * Created by tcd on 25.10.15.
 */
describe('Users resource test', function () {
    'use strict';
    var mockUserResource, mockUserApi, Dima;


    beforeEach(module('app', 'ui.router'));
    beforeEach(inject(function ($injector) {
        mockUserResource = $injector.get('User');
        mockUserApi = $injector.get('MockUserApi');
        Dima = mockUserApi.getDima();
    }));

    it('provider should not be undefined', function () {
        expect(mockUserApi).not.toBeUndefined();
    });
    it('should call findall', function () {
        mockUserResource.findAll(function() {
            expect(result[0].Name).toEqual('Dima');
        });
    });

    it('should call find with id', function () {

        mockUserResource.find({Id: 12}, function () {
            expect(result[0].Id).toEqual('12');
        });
    });

    it('should call remove with id', function () {
        mockUserResource.remove(
            {Id: Dima.Id},
            function () {
                mockUserApi.flush();
            }
        );
    });

    it('should edit and return the edited value', function () {
        mockUserResource.save(
            {Id: Dima.Id, Name: Dima.Name},
            function () {
                expect(result.Id).toEqual(Dima.Id);
            }
        );

    });
});